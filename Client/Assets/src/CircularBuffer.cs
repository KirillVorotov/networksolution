﻿using System;

namespace Networking
{
    public class CircularBuffer<T>
    {
        private T[] items;
        private int start;
        private int end;
        private int count;
        private object lockObject = new object();

        public bool IsEmpty
        {
            get
            {
                lock (lockObject)
                {
                    return count == 0;
                }
            }
        }

        public bool IsNotEmpty
        {
            get
            {
                lock (lockObject)
                {
                    return count != 0;
                }
            }
        }

        public bool IsFull
        {
            get
            {
                lock (lockObject)
                {
                    return count == items.Length;
                }
            }
        }

        public int Capacity
        {
            get
            {
                lock (lockObject)
                {
                    return items.Length;
                }
            }
        }

        public CircularBuffer(int capacity)
        {
            if (capacity <= 0)
                throw new ArgumentException("CircularBuffer cannot have negative or zero capacity.", "capacity");
            items = new T[capacity];
            start = -1;
            end = 0;
            count = 0;
        }

        public CircularBuffer(int capacity, T[] items)
        {
            if (items.Length > capacity)
                throw new ArgumentException("Array length is greater than buffer capacity.");
            this.items = new T[capacity];
            start = -1;
            end = 0;
            count = 0;
            for (int i = 0; i < items.Length; i++)
            {
                this.Push(items[i]);
            }
        }

        public void Push(T item)
        {
            lock (lockObject)
            {
                start++;
                count++;

                if (start == items.Length)
                    start = 0;
                if (count == items.Length + 1)
                {
                    count = items.Length;
                    end++;
                    if (end == items.Length)
                        end = 0;
                }
                items[start] = item;
            }
        }

        public T Take()
        {
            lock (lockObject)
            {
                if (count == 0)
                    throw new Exception("Buffer is empty.");
                var item = items[end];

                end++;
                if (end == items.Length)
                {
                    end = 0;
                }
                count--;
                return item;
            }
        }

        public T Peek()
        {
            lock (lockObject)
            {
                if (count == 0)
                    throw new Exception("Buffer is empty.");
                return items[end];
            }
        }

        public void Clear()
        {
            lock (lockObject)
            {
                start = -1;
                end = 0;
                count = 0;
            }
        }

        public T[] ToArray()
        {
            lock (lockObject)
            {
                T[] result = new T[count];

                for (int i = end; i < count + end; i++)
                {
                    if (i < items.Length)
                        result[i - end] = items[i];
                    else
                        result[i - end] = items[i - items.Length];
                }

                return result;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Rewired;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Trains
{
    public class RewiredInputHandler : MonoBehaviour
    {
        [SerializeField]
        protected bool active = true;
        [SerializeField, Rewired.PlayerIdProperty(typeof(RewiredConsts.Player))]
        protected int playerID;
        [SerializeField, Rewired.ActionIdProperty(typeof(RewiredConsts.Action))]
        protected int actionID;
        [SerializeField]
        protected InputActionEventType actionEventType;
        [SerializeField]
        protected InputAxisEvent onButtonAction;
        protected Player player;

        public bool Active
        {
            get
            {
                return active;
            }
        }

        public void SetActive(bool value)
        {
            active = value;
        }

        public void AddListener(UnityAction<InputActionEventData> call)
        {
            onButtonAction.AddListener(call);
        }

        public void RemoveListener(UnityAction<InputActionEventData> call)
        {
            onButtonAction.RemoveListener(call);
        }

        public void RemoveAllListeners()
        {
            onButtonAction.RemoveAllListeners();
        }

        protected virtual void OnButtonAction(InputActionEventData data)
        {
            if (onButtonAction != null && active)
            {
                onButtonAction.Invoke(data);
            }
        }

        private void OnEnable()
        {
            if (ReInput.isReady)
            {
                player = ReInput.players.GetPlayer(playerID);
            }
            if (player != null && actionID >= 0)
            {
                player.AddInputEventDelegate(OnButtonAction, UpdateLoopType.Update, actionEventType, actionID);
            }
        }

        private void OnDisable()
        {
            if (player != null)
            {
                player.RemoveInputEventDelegate(OnButtonAction);
            }
        }

        [Serializable]
        public class InputAxisEvent : UnityEvent<InputActionEventData> { }
    }
}
